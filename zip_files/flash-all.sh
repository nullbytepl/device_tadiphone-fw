#!/bin/sh

# Copyright 2012 The Android Open Source Project
# Copyright 2020 Tadiphone-CAF
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

echo "TP1803 CAF Vendor by @nullby"
cat release.txt

fastboot flash boot ./update/boot.img
fastboot flash dtbo ./update/dtbo.img
fastboot flash vendor ./update/vendor.img

fastboot flash tz ./update/tz.img
fastboot flash keymaster ./update/keymaster.img
fastboot flash aop ./update/aop.img
fastboot flash bluetooth ./update/bluetooth.img
fastboot flash cmnlib ./update/cmnlib.img
fastboot flash cmnlib64 ./update/cmnlib64.img
fastboot flash dsp ./update/dsp.img
fastboot flash hyp ./update/hyp.img
fastboot flash modem ./update/modem.img
fastboot flash qupfw ./update/qupfw.img
fastboot flash storsec ./update/storsec.img
fastboot flash uefisecapp ./update/uefisecapp.img

sleep 5
echo "YOU MIGHT NEED TO FLASH VBMETA TO BE ABLE TO BOOT!"
