# Clean up working directory
rm -r out 2> /dev/null || true
mkdir -p out

# Generate a release id
if [[ -n $(git status -s -uall) ]]; then
  build_id=$(git rev-parse --abbrev-ref HEAD)-dirty
else
  build_id=$(git rev-parse --abbrev-ref HEAD)-$(git log -1 --format="%at" | xargs -I{} date -d @{} +%d/%m/%y)
fi
echo "Building zip for release $build_id"

# Create release.txt with release id
echo $build_id > out/release.txt

# Copy scripts
cp zip_files/* out/

# Package final zip
echo "Packaging zip... this might take a while"
mkdir -p out/update/
cp cafboot.img out/update/boot.img
cp dtbo.img out/update/dtbo.img
cp vendor.img out/update/vendor.img

cp fw/tz.mbn out/update/tz.img
cp fw/km4.mbn out/update/keymaster.img
cp fw/aop.mbn out/update/aop.img
cp fw/BTFM.bin out/update/bluetooth.img
cp fw/cmnlib.mbn out/update/cmnlib.img
cp fw/cmnlib64.mbn out/update/cmnlib64.img
cp fw/dspso.bin out/update/dsp.img
cp fw/hyp.mbn out/update/hyp.img
cp fw/modem.img out/update/modem.img
cp fw/qupv3fw.elf out/update/qupfw.img
cp fw/storsec.mbn out/update/storsec.img
cp fw/uefi_sec.mbn out/update/uefisecapp.img

cd out/
build_id_normalized=$(echo $build_id | sed -r 's/\///g')
zip -r ./tp1803-vendor-$build_id_normalized.zip * >/dev/null

echo "Done! ./out/tp1803-vendor-$build_id_normalized.zip"
